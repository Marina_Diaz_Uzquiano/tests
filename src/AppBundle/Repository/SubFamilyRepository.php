<?php
/**
 * Created by PhpStorm.
 * User: cfolgoas
 * Date: 25/02/19
 * Time: 16:58
 */

namespace AppBundle\Repository;


use Doctrine\ORM\EntityRepository;

class SubFamilyRepository extends EntityRepository
{
    public function createAlphabeticalQueryBuilder(){
        return $this->createQueryBuilder('sub_family')
            ->orderBy('sub_family.name', 'ASC');
    }
}