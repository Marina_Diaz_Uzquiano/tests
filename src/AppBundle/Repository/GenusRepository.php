<?php
/**
 * Created by PhpStorm.
 * User: cfolgoas
 * Date: 25/02/19
 * Time: 11:17
 */

namespace AppBundle\Repository;


use AppBundle\Entity\Genus;
use Doctrine\ORM\EntityRepository;

class GenusRepository extends EntityRepository
{
    /**
     * @return mixed
     */
    public function findAllPublishedOrderedBySize(){
        return $this->createQueryBuilder('genus')
            ->andWhere('genus.isPublished = :isPublished')
            ->setParameter('isPublished', true)
            ->orderBy('genus.speciesCount', 'DESC')
            ->getQuery()
            ->execute();
    }

}