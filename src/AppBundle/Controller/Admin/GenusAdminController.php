<?php
/**
 * Created by PhpStorm.
 * User: cfolgoas
 * Date: 21/02/19
 * Time: 17:25
 */

namespace AppBundle\Controller\Admin;

use AppBundle\Entity\Genus;
use AppBundle\Form\GenusFormType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;


/**
 * Class GenusAdminController
 * @package AppBundle\Controller\Admin
 * @Route("/admin")
 */
class GenusAdminController extends Controller  // extends to Controller since we're using a service
{

    /**
     * @Route("/genus", name="admin_genus_list")
     */
    public function indexAction()
    {
        $genuses = $this->getDoctrine()
            ->getRepository('AppBundle:Genus')
            ->findAll();

        return $this->render('admin/genus/list.html.twig', array('genuses' => $genuses));
    }

    /**
     * @Route("/toto", name="toto")
     */
    public function totoAction(){

//       Manage data using Session
        $session = new Session();
        $my_name = "Marina";
        $session->set("name", $my_name);

        $var = $session->get("name");

        $other = "Anis";
        dump($var);
        dump($other);


        return $this->render('admin/genus/toto.html.twig', [
            "other" => $other
        ]);
    }

    /**
     * @Route("/tutu", name="tutu")
     */
    public function tutuAction(){

        $savedSession = $this->get("session");

        dump($savedSession->get("name"));



        return $this->render('admin/genus/tutu.html.twig');
    }


    /**
     * @Route("/genus/new", name="admin_genus_new")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function newAction(Request $request)
    {

        //create an object form
        $form = $this->createForm(GenusFormType::class);
        //this is a shortcut method in the vase Controller that calls a method on the form.factor service
        //To create the form we pass to createForm() the GenusFromType::class aka the form type class name


        // handleRequest() renders the form and processes it when the request method is POST
        // handleRequest() grabs the post data off of the $request object for those specific fields and processes them
        $form->handleRequest($request);
        // Once the from submitted we want to do smth with that info, in this case we want to save a new Genus to the DB
        if ($form->isSubmitted() && $form->isValid()) {

            // Doing a quick check by dumping the submitted data and then killing the process
            //dump($form->getData());die;

            $genus = $form->getData();

            $em = $this->getDoctrine()->getManager();
            $em->persist($genus);
            $em->flush();

            $this->addFlash('success', 'Genus created!');

            return $this->redirectToRoute('admin_genus_list');
        }

        //as always, we have to return something as a response in this case we are going to render our form
        // and we are rendering it into a new page called new, cause it's going to be used to sumbit new geni

        return $this->render('admin/genus/new.html.twig', [
            // We pass in the variable genusForm set to $form->createView() to use it to create a form in our GenusFormType file
            // createView() it's a function for form theming
            'genusForm' => $form->createView(),
            'tag' => "new"
        ]);
    }

    /**
     * @Route("/genus/{id}/edit", name="admin_genus_edit")          ////queries for Genus by using the {id} value in Route
     * @param Request $request
     * @param Genus $genus
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editAction(Request $request, Genus $genus)
    {
        $form = $this->createForm(GenusFormType::class, $genus);
        // only handles data on POST
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $genus = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $em->persist($genus);
            $em->flush();
            $this->addFlash('success', 'Genus updated!');
            return $this->redirectToRoute('admin_genus_list');
        }
        return $this->render('admin/genus/edit.html.twig', [
            'genusForm' => $form->createView(),
            'tag' => "update"
        ]);
    }

}