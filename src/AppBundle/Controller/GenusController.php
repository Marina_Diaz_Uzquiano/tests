<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Genus;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Repository\GenusRepository;



class GenusController extends Controller
{
//    /**
//     * @Route("/genus/new", name="")
//     */
//    public function newAction(){
//        $genus = new Genus();
//        $genus->setName('Octopus'.rand(1,100));
//
//        $em = $this->getDoctrine()->getManager();
//        $em->persist($genus);
//        $em->flush();
//
//        return new Response('<html><body>Genus created!</body></html>');
//
//    }


// HOW TO QUERY THROUGH THE SCRIPTS
    /**
     * @Route("/genus")
     */
    public function listAction()
    {
        $session = $this->get("session");

        $name = $session->get("name");

        dump($session->get("name"));
        // as always we need to call the Entity Manager
        $em = $this->getDoctrine()->getManager();

        // inside the entity manager we search for the gerRepository function and the findAll funciton
//        $genuses=$em->getRepository('AppBundle\Entity\Genus')
        $genuses = $em->getRepository('AppBundle:Genus')// it's a Doctrine shortcut (when creating extra folders inside a main one, try not to name the same to files belonging to the same main folder)
        ->findAllPublishedOrderedBySize();

//        // To see what this two functions together do, let's do a dump
//        dump($genuses);die;
//        // 1st getRepository() goes to the pathway specified as argument (always a file), and then findAll searches every object inside that pathway (file)

        return $this->render('genus/list.html.twig', [
            'genuses' => $genuses,
            "name" => $name
        ]);

    }


    /**
     * @Route("/test", name="test_page")
     *
     */
    public function testAction()
    {

        $genus = new Genus();
        $genus->setName("toto");
        $genus->setSubFamily("dunno");


        $result = $genus->__toString();

        $this->getDump($result);
//        dump($result);die;

    }

    /**
     * @Route("/genus/{genusName}", name="genus_show")
     */
    public function showAction($genusName)
    {
//        return $this->render('genus/show.html.twig', array(
//            'name' => $genusName,

        $em = $this->getDoctrine()->getManager();

        $genus = $em->getRepository('AppBundle:Genus')
            ->findOneBy(['name' => $genusName]); // passing an array to findOneBy of the things we want to find in our case a name saved in the variable genusName
        if (!$genus) {
            throw $this->createNotFoundException('genus not found');
        }

        return $this->render('genus/show.html.twig',
            array('genus' => $genus));

    }


    /**
     * @Route("/genus/{genusName}/notes",
     *     name="genus_show_notes",
     *     methods={"GET"})
     */
    public function getNotesAction()
    {
        $notes = [
            ['id' => 1, 'username' => 'AquaPelham', 'avatarUri' => '/images/leanna.jpeg', 'note' => 'Octopus asked me a riddle, outsmarted me', 'date' => 'Dec. 10, 2015'],
            ['id' => 2, 'username' => 'AquaWeaver', 'avatarUri' => '/images/ryan.jpeg', 'note' => 'I counted 8 legs... as they wrapped around me', 'date' => 'Dec. 1, 2015'],
            ['id' => 3, 'username' => 'AquaPelham', 'avatarUri' => '/images/leanna.jpeg', 'note' => 'Inked!', 'date' => 'Aug. 20, 2015'],
        ];
        $data = [
            'notes' => $notes,
        ];
        return new JsonResponse($data);


    }


    public function getDump($var)
    {
        dump($var);
        die;
    }
}