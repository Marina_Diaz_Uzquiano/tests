<?php
/**
 * Created by PhpStorm.
 * User: cfolgoas
 * Date: 22/02/19
 * Time: 17:59
 */

namespace AppBundle\DataFixtures\ORM;


use AppBundle\Entity\Genus;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Nelmio\Alice\Fixtures;
use Symfony\Component\Debug\Tests\Fixtures\InternalInterface;
use Doctrine\Bundle\FixturesBundle;

class LoadFixtures extends FixturesBundle\Fixture
    //implements InternalInterface
{
    public function load(ObjectManager $manager){
        $objects = Fixtures::load(__DIR__ . '/GenusFixtures.yaml', $manager, ['providers'=>[$this]]);
    }

    public function genus(){
        $genera = [
            'Octopus',
            'Balaena',
            'Orcinus',
            'Hippocampus',
            'Asterias',
            'Amphiprion',
            'Carcharodon',
            'Aurelia',
            'Cucumaria',
            'Balistoides',
            'Paralithodes',
            'Chelonia',
            'Trichechus',
            'Eumetopias'
        ];
        $key = array_rand($genera);
        return $genera[$key];

    }

}