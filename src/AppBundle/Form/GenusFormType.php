<?php
/**
 * Created by PhpStorm.
 * User: cfolgoas
 * Date: 21/02/19
 * Time: 16:55
 */

namespace AppBundle\Form;


use AppBundle\Entity\SubFamily;
use AppBundle\Repository\SubFamilyRepository;
//use Doctrine\DBAL\Types\DateType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class GenusFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options){

        $builder
            ->add('name')
            ->add('subFamily', EntityType::class, [
                'required' => false,
                'placeholder' => 'Choose a Sub-family',
                'class' => SubFamily::class,
//                we want to order alphabetically => We need to call query_builder
                'query_builder' => function(SubFamilyRepository $repo){
                    return $repo->createAlphabeticalQueryBuilder();
                }])

            ->add('speciesCount')
            ->add('funFact')
            ->add('isPublished', ChoiceType::class, [
                'choices' => [
                    'Yes' => true,
                    'No' => false,
                ]
            ])
            ->add('firstDiscoveredAt', DateType::class, [
                'widget' => 'single_text',
                'attr' => ['class' => 'js-datepicker'],
                'html5' => false,
            ]);

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'AppBundle\Entity\Genus'
        ]);
        parent::configureOptions($resolver);
    }
}