<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20190225140615 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE genus CHANGE sub_family sub_family INT DEFAULT NULL');
        $this->addSql('ALTER TABLE genus ADD CONSTRAINT FK_38C5106E17B76E17 FOREIGN KEY (sub_family) REFERENCES sub_family (id)');
        $this->addSql('CREATE INDEX IDX_38C5106E17B76E17 ON genus (sub_family)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE genus DROP FOREIGN KEY FK_38C5106E17B76E17');
        $this->addSql('DROP INDEX IDX_38C5106E17B76E17 ON genus');
        $this->addSql('ALTER TABLE genus CHANGE sub_family sub_family VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci');
    }
}
